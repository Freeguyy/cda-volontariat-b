package fr.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VolontariatBack1Application {

	public static void main(String[] args) {
		SpringApplication.run(VolontariatBack1Application.class, args);
	}

}
